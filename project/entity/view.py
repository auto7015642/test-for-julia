from pydantic import BaseModel


class View(BaseModel):
    cat: str
    desc: str
    id: int
    img: str
    price: int
    title: str
