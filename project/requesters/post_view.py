import requests
import config
from project.entity.view import View

URL = "view"
data = {
    "id": "3"
}


def post_data() -> View:
    response = requests.post(f"{config.DOMAIN}{URL}", json=data)
    return View(**response.json())

